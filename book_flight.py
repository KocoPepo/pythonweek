#!/usr/bin/env python3

import getopt
import json
import logging

import sys

from booking import book
from flight import getFlight

args = {'date' : None,
        'from' : None,
        'to' : None,
        'one-way': False,
        'return': None,
        'cheapest': False,
        'shortest': False}


def usage():
    print(
    """
author: Martin Kocour
project: Python Week at Kiwi

usage:
./book_flight.py --date 2017-10-13 --from BCN --to DUB --one-way
./book_flight.py --date 2017-10-13 --from LHR --to DXB --return 5
./book_flight.py --date 2017-10-13 --from NRT --to SYD --cheapest
./book_flight.py --date 2017-10-13 --from CPH --to MIA --shortest
    """)


def parse_arguments():
    shortopts = ""
    longopts = ["date=", "from=", "to=", "one-way", "return=", "cheapest", "shortest"]

    try:
        opts, args_left = getopt.getopt(sys.argv[1:], shortopts, longopts)
    except getopt.GetoptError as err:
        logging.error("Could not parse arguments: %s", err)
        usage()
        sys.exit(2)

    for o, a in opts:
        args[o[2:]] = a


parse_arguments()

response_body = getFlight(args)
json = json.loads(response_body)
data = json['data']
if len(data) > 0 :
    book(data[0]['booking_token'])
else:
    print("Could nout found the flight")
