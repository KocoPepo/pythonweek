from datetime import datetime
import urllib.request
import urllib.error

import logging

import sys


class RequestBuilder:
    url = "https://api.skypicker.com/flights?"

    def add(self, param, value):
        sign = '&'
        if self.url[-1] == '?':
            sign = ""
        if isinstance(value, bool) and value is True:
            self.url = self.url + sign + param
        elif isinstance(value, bool) is False and value is not None:
            self.url = self.url + sign + param + '=' + value

        return self

    def create(self):
        request = urllib.request.Request(self.url)
        return request


def format_date(dateString):
    date = datetime.strptime(dateString, "%Y-%m-%d")
    return date.strftime("%d%%2F%m%%2F%Y")


def getFlight(param: dict):
    builder = RequestBuilder()
    date = format_date(param['date'])
    builder.add('dateFrom', date) \
        .add('dateTo', date) \
        .add('flyFrom', param['from']) \
        .add('to', param['to'])

    if param['one-way'] is False:
        builder.add('daysInDestinationFrom', param['return']) \
            .add('daysInDestinationTo', param['return'])

    if param['shortest']:
        builder.add('sort', 'duration')
    else:
        builder.add('sort', 'price')

    request = builder.create()
    try:
        return urllib.request.urlopen(request).read()
    except urllib.error.URLError as err:
        logging.error("Could not get flights: %s", err)
        sys.exit(2)
