import json
import urllib.request
import urllib.parse
import urllib.error

import logging

passenger = {
    'firstName': "Aron",
    'documentID': "EA123321",
    'birthday': "1993-11-02",
    'email': "aron.schwarz@reddit.com",
    'title': "Mr",
    'lastName': "Schwarz"
}

booking = {
    'currency': "CZK",
    'booking_token': "",
    'passengers': []
}

url = "http://37.139.6.125:8080/booking"
headers = {
    'Content-Type' : "application/json"
}


def book(booking_token):
    booking['booking_token'] = booking_token
    booking['passengers'].append(passenger)
    data = json.dumps(booking).encode("UTF-8")
    request = urllib.request.Request(url, data=data, headers=headers, method="POST")
    try:
        response = urllib.request.urlopen(request).read()
        jsonVal = json.loads(response)
        if 'pnr' in jsonVal and jsonVal['status'].lower() == 'confirmed':
            print("PNR: " + jsonVal['pnr'])
        else:
            print("Could not book the flight")
    except urllib.error.URLError as err:
        logging.error("Could not get booking: %s,", err)

